// React Component for Video Background
import React from 'react';
import { useLocation } from 'react-router-dom';
import video from '../../assets/Video_2.mp4';

const VideoBackground = () => {
  // get page location
  const location = useLocation();

  return (
    <>
      <video
        autoPlay
        loop
        muted
        className={location.pathname === '/' ? '' : 'not-front-page'}
      >
        <source src={video} type="video/mp4" />
        Video not supported.
      </video>
    </>
  );
};

export default VideoBackground;
