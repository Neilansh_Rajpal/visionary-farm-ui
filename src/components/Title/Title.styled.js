// Styles for Title using styled-components
import styled, { keyframes } from 'styled-components';
import { flipInX } from 'react-animations'; // animation for title (only happens when the FrontPage Component is rendered)

import TitleGif from '../../assets/Title.gif'; // gif used in App Title

const TitleAnimation = keyframes`${flipInX}`;

export const TitleWrapper = styled.div`
  margin-bottom: 10px;
  border: black;

  &.on-front-page {
    // animation for the whole Title Component (including subtitles)
    animation: 1 3s ${TitleAnimation};
    color: black;
  }

  // class used to hide the Title Component when the user navigates to Launch, Settings, or Help page
  &.hide {
    visibility: hidden;
  }

  h1 {
    background-image: url(${TitleGif});
    color: transparent;
    -moz-background-clip: text;
    -webkit-background-clip: text;
    background-clip: text;
    font-family: 'Anton', sans-serif;
    text-transform: uppercase;
  }

  // animation for subtitle
  @keyframes reveal {
    0% {
      opacity: 0;
    }
    75% {
      opacity: 0.9;
    }
    100% {
      opacity: 1;
    }
  }

  h4 {
    color: black;
    font-family: 'Raleway', sans-serif;
    margin-top: 30px;
    display: inline-block; // to have the 2 h4 elements in the same line
  }

  // first h4 element
  #sub1 {
    margin-right: 5px; // to have some space between the 2 h4 elements (since they're printed on the same line)
  }

  // second h4 element
  #sub2 {
    animation: reveal 12s infinite;
  }
`;
