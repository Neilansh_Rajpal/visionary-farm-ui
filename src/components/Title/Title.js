// React Component for Title
import React from 'react';
import { useLocation } from 'react-router';
import { TitleWrapper } from './Title.styled';

const Title = ({ title, subtitle1, subtitle2 }) => {
  // get page location
  const location = useLocation();

  return (
    <TitleWrapper
      className={
        location.pathname === '/' || location.pathname === '/menu'
          ? 'on-front-page'
          : 'hide'
      }
    >
      <h1>{title}</h1>
      <h4 id="sub1">
        {subtitle1}
        <br />
        <span id="sub2"> {subtitle2}</span>
      </h4>
    </TitleWrapper>
  );
};

export default Title;
