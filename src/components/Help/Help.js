// React Component for the Help page of the App
import React from 'react';
import { useHistory } from 'react-router'; // to get previous page

import BackButton from '../BackButton/BackButton'; // custom-Component for Back Button
import { ComponentWrapper } from '../../styles/styled'; // wrapper for animation

const Help = () => {
  // get history
  const history = useHistory();

  return (
    <ComponentWrapper className="content-item mx-5 w-4/5 md:w-3/5 mx-auto lg:w-1/2">
      <h1 className="text-4xl flex  sm:text-5xl">
        {' '}
        <BackButton history={history} /> Help
      </h1>
      <p>
        Got stuck? Have an issue to report? Reach out to us at +1 778-723-2410.
        Our offices are open Mon-Fri from 9 to 5 PM PST.
      </p>
    </ComponentWrapper>
  );
};

export default Help;
