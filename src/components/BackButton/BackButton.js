// React Component for Back Button used across different Components across the App.
import React from 'react';
import { IconButton, Icon } from 'rsuite';

const BackButton = ({ history }) => {
  return (
    <>
      <IconButton
        className="mx-2 hover:bg-gray-500"
        icon={<Icon icon="back-arrow" />}
        onClick={() => history.goBack()}
        color="blue"
        size="lg"
        appearance="ghost"
      />
    </>
  );
};

export default BackButton;
