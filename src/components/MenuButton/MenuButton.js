// React Component for Buttons on the Main Page of the App
import React from 'react';
import { Button, Icon } from 'rsuite';
import { ButtonWrapper } from './MenuButton.styled';

const MenuButton = ({ text, iconName }) => {
  return (
    <ButtonWrapper>
      <Button block size="lg" className="text-2xl">
        <Icon icon={iconName} size="2x" /> {text}
      </Button>
    </ButtonWrapper>
  );
};

export default MenuButton;
