// styles for Menu Button using styled-components
import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  // later add this to styled.js (yet to create) so that other buttons can use this
  margin-bottom: 20px;

  Button {
    background-image: linear-gradient(to right, #0066b3, #00ffbf);
    color: white;
    opacity: 1;

    &:hover {
      color: black;
      background-image: linear-gradient(to right, #00ffbf, #0066b3);
    }
  }
`;
