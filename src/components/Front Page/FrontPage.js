// React Component for Front Page appearing when the App is first opened
import React, { useState, useEffect, useRef } from 'react';
import { Redirect } from 'react-router-dom';

import { FrontPageWrapper } from './FrontPage.styled';
import logo from '../../assets/Logo 11.png';

const FrontPage = () => {
  // state to keep track of animation
  const [animated, setAnimated] = useState();

  // reference for VisionBox image
  const imgRef = useRef();

  return (
    <FrontPageWrapper>
      <div
        id="img-container"
        className="p-8 w-1/2"
        ref={imgRef}
        style={{ display: 'block' }}
        onAnimationEnd={() => {
          // once animation ends, hide the VisionBox image
          imgRef.current.style.display = 'none';
          setAnimated('true'); // implying animation is over
        }}
      >
        <img src={logo} alt="logo" className="h-40 w-80 sm:h-48 w-32 md:w-80" />
      </div>
      {animated && <Redirect to="/menu" />}
    </FrontPageWrapper>
  );
};

export default FrontPage;
