// Styles for the Front Page Component using styled-components
import styled, { keyframes } from 'styled-components';
import { flipOutX, fadeInDown } from 'react-animations'; // animation for image

const imageFlipOutAnimation = keyframes`${flipOutX}`; // defining animation using keyframes (just like keyframe from css)
const divFlipInDOwnAnimation = keyframes`${fadeInDown}`; // animation for button

export const FrontPageWrapper = styled.div`
  margin-top: 50px;
  padding: 10px;

  #front-page-btn {
    animation: 1 5s ${divFlipInDOwnAnimation};
  }

  img {
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
    animation: 1 8s ${imageFlipOutAnimation};
  }
`;

export const FrontPageButtonWrapper = styled.div`
  animation: 1 1s ${divFlipInDOwnAnimation};
`;
