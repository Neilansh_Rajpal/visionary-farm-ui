// React Component for the Homepage of the App
import React from 'react';
import { Link } from 'react-router-dom';
import MenuButton from '../MenuButton/MenuButton';
import { ComponentWrapper } from '../../styles/styled'; // wrapper for animation

// links for each page
const PAGES = [
  { to: '/launch', linkName: 'Launch', iconName: 'rocket' },
  { to: '/settings', linkName: 'Settings', iconName: 'setting' },
  { to: '/help', linkName: 'Help', iconName: 'help-o' },
];

const MainMenu = () => {
  return (
    <ComponentWrapper className="content-item mx-5 mt-8 w-4/5 sm:w-3/5 mx-auto md:w-1/2 lg:w-2/5 xl:w-1/3">
      {
        // links to each page
        PAGES.map(page => (
          <Link key={page.to} to={page.to} style={{ textDecoration: 'none' }}>
            <MenuButton text={page.linkName} iconName={page.iconName} />
          </Link>
        ))
      }
    </ComponentWrapper>
  );
};

export default MainMenu;
