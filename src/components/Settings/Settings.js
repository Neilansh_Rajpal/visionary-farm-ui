// React Component for Settings page of the App
import React from 'react';
import { useHistory } from 'react-router'; // to go to the previous page
import { Divider, Toggle } from 'rsuite';

import BackButton from '../BackButton/BackButton'; // custom-Component for Back Button
import { ComponentWrapper } from '../../styles/styled'; // wrapper for animation
import { SettingsMenu } from './Settings.styled';

// dummy settings
const SETTINGS = [
  { name: 'Setting 1', checked: true },
  { name: 'Setting 2', checked: false },
  { name: 'Setting 3', checked: true },
  { name: 'Setting 4', checked: true },
  { name: 'Setting 5', checked: false },
];

const Settings = () => {
  // get history
  const history = useHistory();

  return (
    <ComponentWrapper
      className="content-item mx-5 w-4/5 sm:w-3/4 mx-auto md:w-3/5 lg:w-1/2"
      style={{ wordWrap: 'break-word' }}
    >
      <h1 className="text-4xl flex  sm:text-5xl">
        {' '}
        <BackButton history={history} /> Settings
      </h1>

      <SettingsMenu>
        {SETTINGS.map(setting => (
          <div key={setting.name}>
            <div className="text-left flex">
              <p className="m-0">{setting.name} </p>
              <Toggle
                key={setting.name}
                defaultChecked={setting.checked}
                size="lg"
                className="ml-auto float-right"
              />
            </div>
            <Divider />
          </div>
        ))}
      </SettingsMenu>
    </ComponentWrapper>
  );
};

export default Settings;
