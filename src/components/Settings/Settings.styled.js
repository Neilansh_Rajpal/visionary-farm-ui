// styles for Settings page using styled-components
import styled from 'styled-components';

export const SettingsMenu = styled.div`
  margin-top: 20px;
  text-align: left;
`;
