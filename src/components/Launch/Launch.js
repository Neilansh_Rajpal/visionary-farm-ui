// React Component for the Launch page of the App.
import React from 'react';
import { useHistory } from 'react-router'; // to go back previous page

import './Launch.css';
import BackButton from '../BackButton/BackButton';
import { ComponentWrapper } from '../../styles/styled'; // wrapper for animation
import farmImg from '../../assets/farm.jpg';

const Launch = () => {
  // get link to previous page
  const history = useHistory();

  const slitImageEffect = () => {
    const imgContainer = document.getElementById('image-container'); // div container containing image

    // get image container height and width
    const height = imgContainer.offsetHeight;
    const width = imgContainer.offsetWidth;

    // number of grids/slices x and y-direction
    const slicesX = 4;
    const slicesY = 1;

    // get image source
    const imgSrc = document.getElementById('image').src;

    // eslint-disable-next-line no-console
    // console.log(width, height);

    /* 
      - for each slice/grid, define a div element (this will contain slice of image)
      - width and height of the div element depends on number of slices
      - assign each div a background image of the original image
      - background-position depends on width of each slice
      - slicesHeight = original image height / num of vertical slices (slicesY) 
      - slicesWidth = original image width / num of horizontal slices (slicesX) 
    */

    const sliceHeight = height / slicesY;
    const sliceWidth = width / slicesX;

    for (let y = 0; y < slicesY; y++) {
      for (let x = 0; x < slicesX; x++) {
        const sliceDiv = document.createElement('div');

        sliceDiv.style.height = `${sliceHeight}px`;
        sliceDiv.style.width = `${sliceWidth}px`;

        sliceDiv.style.left = `${x * sliceWidth}px`;
        sliceDiv.style.top = 0;

        // assign background image to the div
        sliceDiv.style.backgroundImage = `url(${farmImg})`;
        sliceDiv.style.backgroundSize = `${width}px`;
        sliceDiv.style.backgroundPosition = `${-1 * x * sliceWidth}px`;

        sliceDiv.classList.add('grayscale');
        sliceDiv.classList.add('slice'); // class defined in Launch.css for styles for image slices

        // appending the div to the image-container div
        imgContainer.appendChild(sliceDiv);

        // eslint-disable-next-line no-console
        // console.log(sliceDiv);
      }
    }
    // eslint-disable-next-line no-console
    // console.log(imgContainer.children);
  };

  return (
    <ComponentWrapper className="content-item fix-width">
      <h1 className="flex text-5xl">
        <BackButton history={history} /> Launch
      </h1>

      <div
        id="image-container"
        className="flex mt-3"
        onLoad={() => {
          slitImageEffect();
        }}
      >
        <img id="image" src={farmImg} alt="" />
      </div>
    </ComponentWrapper>
  );
};

export default Launch;
