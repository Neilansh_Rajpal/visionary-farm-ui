// Styles written with styled-components used across different Components of the app
import styled, { keyframes } from 'styled-components';
import { zoomIn } from 'react-animations'; // animation for the Settings Menu

const zoomInAnimation = keyframes`${zoomIn}`;

/* 
    - a wrapper containing only animation.
    - used for MainMenu, Launch, Settings and Help Components.
    - had to create in a separate file since react-animations doesn't work inside a css file.
    - so created a component-wrapper that only has animation as its style.
    - other styles have been defined exclusively for each Component in their respective files.
*/
export const ComponentWrapper = styled.div`
  animation: 1 1s ${zoomInAnimation};
`;
