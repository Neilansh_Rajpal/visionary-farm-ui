import React from 'react';
import 'rsuite/dist/styles/rsuite-default.css'; // to fix rsuite related issues
import { Route } from 'react-router-dom'; // to navigate to different pages

import Title from './components/Title/Title';
import VideoBackground from './components/VideoBackground/VideoBackground';
import FrontPage from './components/Front Page/FrontPage';
import MainMenu from './components/Main Menu/MainMenu';
import Launch from './components/Launch/Launch';
import Settings from './components/Settings/Settings';
import Help from './components/Help/Help';

const routes = [
  { path: '/', Component: FrontPage },
  { path: '/menu', Component: MainMenu },
  { path: '/launch', Component: Launch },
  { path: '/settings', Component: Settings },
  { path: '/help', Component: Help },
];

function App() {
  return (
    <>
      <VideoBackground />
      <div className="content">
        <Title
          title="Visionary Farm"
          subtitle1="Making farming"
          subtitle2="amiable."
        />
        {/* To match the paths with their respective Components */}
        {routes.map(route => (
          <Route key={route.path} exact path={route.path}>
            <route.Component />
          </Route>
        ))}
      </div>
    </>
  );
}

export default App;
